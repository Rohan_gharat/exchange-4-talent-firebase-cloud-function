/**
 * Import function triggers from their respective submodules:
 *
 * const {onCall} = require("firebase-functions/v2/https");
 * const {onDocumentWritten} = require("firebase-functions/v2/firestore");
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

const functions = require('firebase-functions');
const { onRequest } = require("firebase-functions/v2/https");
const logger = require("firebase-functions/logger");

const admin = require('firebase-admin');
admin.initializeApp();

const firestore = admin.firestore();

// exports.myFunction = functions.runWith({ maxInstances: 10 }).https.onRequest((request, response) => {
//     // logger.info("Hello logs!", { structuredData: true });
//     response.send("Hello from Firebase!");
// });

// exports.chatAdded =functions.firestore.document('secretService/{document}/chats').onCreate((snapshot,context)=>{
//     console.log(snapshot.data());
//     return Promise.resolve();
// });

// exports.chatUpdate =functions.firestore.document('secretService/{document}/chats').onUpdate((snapshot,context)=>{
//     console.log('Before ',snapshot.before.data());
//     console.log('After ',snapshot.after.data());
//     return Promise.resolve();
// });

// exports.chatDeleted =functions.firestore.document('secretService/{document}/chats').onDelete((snapshot,context)=>{
//     console.log(snapshot.data(),'deleted');
//     return Promise.resolve();
// });


exports.schudeluerFunction = functions.pubsub
  .schedule('* * * * *')
  .onRun(async (context) => {
    console.log('I am running every minute');
    
    try {
        const now = Date.now();
        const groupsSnapshot = await firestore.collection('secretServices').get();
        console.log('date:', now);
        const deletePromises = [];
  
        groupsSnapshot.forEach((groupDoc) => {
            console.log('secretService foreach start');
          const chatsCollectionRef = groupDoc.ref.collection('chats');
          
          chatsCollectionRef.get().then((chatsSnapshot) => {
            chatsSnapshot.forEach((chatDoc) => {
                console.log('chats foreach start');
              const { timeSent, message } = chatDoc.data();
              const timeDifference = now - timeSent;
              console.log('chats doc message', message);
              // Check if the document is older than 5 minutes (5 * 60 * 1000 milliseconds)
              if (timeDifference > 5 * 60 * 1000) {
                console.log('Group:', groupDoc.id, 'Document message before deletion:', message);
                deletePromises.push(chatDoc.ref.delete());
              }
            });
          });
          
        });
  
        // Execute all delete promises in parallel
        await Promise.all(deletePromises);
  
        console.log('Old chat documents deleted successfully.');
        return null;
      } catch (error) {
        console.error('Error checking old chat documents:', error);
        throw new Error('Failed to check old chat documents');
      }
  });

